package com.tda.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.iris.tda.controller.models.Customer;
import com.iris.tda.model.CustomerApiException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
	Customer customer;
	@ExceptionHandler(CustomerApiException.class)
	public final  ResponseEntity<Object> handleAllExceptions(CustomerApiException ex){
		ex.setMessage("Server Error:");
		ex.setDetail("Their is Something error in Server");
		ex.setErrorCode("500");
		CustomerApiException exception=new CustomerApiException(ex.getMessage(),ex.getDetail(),ex.getErrorCode());
		return new ResponseEntity<Object>(exception,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
//	@ExceptionHandler(RecordNotFoundExcpetion.class)
//	public final ResponseEntity<Object> handleUserNotFoundException(RecordNotFoundExcpetion exception,WebRequest request){
//		List<String> list=new ArrayList<>();
//		list.add(exception.getLocalizedMessage());
//		CustomerServiceResponse error=new CustomerServiceResponse("Record Not Found",list);
//		return new ResponseEntity<Object>(error,HttpStatus.NOT_FOUND);
//	}
//	
}
