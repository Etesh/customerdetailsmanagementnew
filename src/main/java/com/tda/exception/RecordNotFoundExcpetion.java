package com.tda.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus
public class RecordNotFoundExcpetion extends RuntimeException{
	public RecordNotFoundExcpetion(String exception) {
		super(exception);
	}
}
