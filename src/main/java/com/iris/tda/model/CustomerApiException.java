package com.iris.tda.model;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class CustomerApiException extends RuntimeException {
	private List<Error> errors=null;
	
	public CustomerApiException() {
		errors=new ArrayList<Error>();
	}
	
	public CustomerApiException(final List<Error> errors) {
		this.errors=new ArrayList<Error>(errors);
	}
	
	public CustomerApiException(final String errorName,final String errorMessage) {
		errors=new ArrayList<Error>();
		
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}
	
	

	
	private String errorCode;
	private String message;
	private String detail;
	public CustomerApiException(String errorCode, String message, String detail) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.detail = detail;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	@Override
	public String toString() {
		return "CustomerApiException [errorCode=" + errorCode + ", message=" + message + ", detail=" + detail + "]";
	}
	
	
	
}
