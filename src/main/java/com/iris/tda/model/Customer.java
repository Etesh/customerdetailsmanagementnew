package com.iris.tda.model;

import lombok.Data;

/**
 * Model class having attributes of Customer.
 * Have a parameterized and non-parameterized constructor.
 * Have a getter and setter method.
 * Have a toString() method.
 * */
@Data
public  class Customer {
	private int id;
	
	private String name;
	
	private int age;
	
	private String accountnumber;
	
	private String address;
	
	private String role;

	public Customer() {

	}

	public Customer(int id, String name, int age, String accountnumber, String address, String role) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.accountnumber = accountnumber;
		this.address = address;
		this.role = role;
	}

}
