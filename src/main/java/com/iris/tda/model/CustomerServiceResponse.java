package com.iris.tda.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

/**
 *This is Response Error page.
 *It adds Error if occurs in the application.
 * **/
public class CustomerServiceResponse {

	Customer customer;
	private String Message;
	
	List<String> errors;
	
	public void addError(String error) {
		errors.add(error);
	}
	
	public boolean hasError() {
		return !CollectionUtils.isEmpty(errors);
	}
    public CustomerServiceResponse() {
		errors=new ArrayList<>();
	}
	

	public CustomerServiceResponse(String Message, List<String> errors) {
		//this.customer = customer;
		this.setMessage(Message);
		this.errors = errors;
	}


	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "CustomerServiceResponse [customer=" + customer + ", errors=" + errors + "]";
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
	
}
