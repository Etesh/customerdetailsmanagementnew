package com.iris.tda.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iris.tda.model.Customer;
import com.iris.tda.model.CustomerApiException;

/**
 * This is a interface of CustomerDetailController consist of various end point to perform CURD operation for Customer Detail.
 * 
 * **/
public interface CustomerDetailsApi  {
	@RequestMapping(method=RequestMethod.GET,value="/customers")
	public ResponseEntity<List<Customer>> ViewAllCustomer() throws CustomerApiException;
	
	@RequestMapping(method=RequestMethod.POST,value="/customer")
	public ResponseEntity<?> addCustomer(@RequestBody @Valid Customer customer) throws CustomerApiException;

	@RequestMapping(method=RequestMethod.PUT,value="/customer")
	public ResponseEntity<?> updateCustomer(@RequestBody  @Valid Customer customer) throws CustomerApiException;
	
	@RequestMapping(method=RequestMethod.DELETE,value="/customer/{id}")
	public ResponseEntity<?> deleteCustomer(@PathVariable int id) throws CustomerApiException;

}