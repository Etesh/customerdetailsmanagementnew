package com.iris.tda.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iris.tda.controller.CustomerDetailsApi;
import com.iris.tda.model.Customer;
import com.iris.tda.model.CustomerApiException;
import com.iris.tda.model.CustomerServiceResponse;
import com.iris.tda.service.impl.CustomerServiceImpl;

/**
 * This is Controller class that implements all the end point or rest services
 * that is declared in CustomerDetailApi
 **/
@RestController
public class CustomerDetailsController implements CustomerDetailsApi {

	@Autowired
	private CustomerServiceImpl customerService;

	/**
	 * This end point or method is used to get all the customerDetails inserted into
	 * database.
	 * 
	 * @return list of customer if HttpStatus is 200 OK.
	 **/

	@Override
	public ResponseEntity<List<Customer>> ViewAllCustomer() throws CustomerApiException {
		List<Customer> customers = customerService.ViewAllCustomer();
		return new ResponseEntity<>(customers, HttpStatus.OK);
	}

	/**
	 * This method is used to insert the customer with their details into database.
	 * 
	 * @return Response Error if Http Status is 400 that is BAD_REQUEST.
	 * @return response error null and insert customer into database if Http status
	 *         is 200 OK.
	 **/

	@Override
	public ResponseEntity<?> addCustomer(@RequestBody Customer customer) throws CustomerApiException {
		CustomerServiceResponse insertedCustomerResponse = customerService.addCustomer(customer);
		if (insertedCustomerResponse.hasError()) {
			return new ResponseEntity<>(insertedCustomerResponse.getErrors().toString(), HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	/**
	 * This method is used to update the Customer details and then insert into
	 * database.
	 * 
	 * @return Response Error if Http Status is 400 that is BAD_REQUEST.
	 * @return Response Error null and update customer details into database if Http
	 *         status is 200 OK.
	 **/

	@Override
	public ResponseEntity<?> updateCustomer(@RequestBody Customer customer) throws CustomerApiException {
		CustomerServiceResponse updateCustomerResponse = customerService.updateCustomer(customer);
		if (updateCustomerResponse.hasError()) {
			return new ResponseEntity<>(updateCustomerResponse.getErrors(), HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	/**
	 * This method is used to delete customer from database.
	 * 
	 * @return Response Error if Http Status is 400 that is BAD_REQUEST.
	 * @return Response Error null and delete customer from database if Http status
	 *         is 200 OK.
	 **/

	@Override
	public ResponseEntity<?> deleteCustomer(@PathVariable int id) throws CustomerApiException {
		CustomerServiceResponse deleteCustomer = customerService.deleteCustomer(id);
		if (deleteCustomer.hasError()) {
			return new ResponseEntity<>(deleteCustomer.getErrors().toString(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);

	}

}
