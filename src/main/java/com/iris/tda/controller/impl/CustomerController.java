package com.iris.tda.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iris.tda.controller.CustomerDetailsController;
import com.iris.tda.model.Customer;
import com.iris.tda.service.impl.CustomerServiceImpl;

@RestController
public class CustomerController implements CustomerDetailsController {
 
	@Autowired
	 private CustomerServiceImpl customerService;

	public ResponseEntity<List<Customer>> ViewAllCustomer(){
		List<Customer> customers=customerService.ViewAllCustomer();
		return new ResponseEntity<>(customers,HttpStatus.OK) ;
	}
	
	public ResponseEntity<?> addCustomer(@RequestBody Customer cust) {
		String insertedCustomer=customerService.addCustomer(cust);
		return new ResponseEntity<>(insertedCustomer,HttpStatus.OK);
	
	}
	
	public ResponseEntity<?> updateCustomer(@RequestBody Customer c) {
		String updateCustomer=customerService.updateCustomer(c);
		return new ResponseEntity<>(updateCustomer,HttpStatus.OK);
		
	}
	
	public ResponseEntity<?> deleteCustomer(@PathVariable int id) {
		String deleteCustomer=customerService.deleteCustomer(id);
		return new ResponseEntity<>(deleteCustomer,HttpStatus.FORBIDDEN);
		
	}
	
}
