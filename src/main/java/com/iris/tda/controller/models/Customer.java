package com.iris.tda.controller.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

/**
 * This class representing model for CustomerDetailController..
 * It consists attributes of customer to perform CRUD operation.
 * **/
@Data
public class Customer {
private int id;
	
	@NotNull
	@Size(min=1,max=40)
	private String name;
	
	@NotNull
	private int age;
	
	@NotNull
	@Size(min=1,max=16)
	private String accountNumber;
	
	@NotNull
	private String address;
	
	@NotNull
	private String role;
	
	public Customer() {
		
	}
	
	public Customer(int id, String name, int age, String accountnumber, String address, String role) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.accountNumber = accountnumber;
		this.address = address;
		this.role = role;
	}	

}
