package com.iris.tda.service;

import java.util.List;

import com.iris.tda.model.Customer;
import com.iris.tda.model.CustomerServiceResponse;
/**
 * This is service interface having CRUD  method that is implemented in customerserviceimpl class.
 *
 * */
public interface CustomerService {
	public List<Customer> ViewAllCustomer();
	public CustomerServiceResponse addCustomer(Customer cust);
	public CustomerServiceResponse updateCustomer(Customer c);
	public CustomerServiceResponse deleteCustomer(int id);
}
