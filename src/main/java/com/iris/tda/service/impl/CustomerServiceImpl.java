package com.iris.tda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iris.tda.dao.CustomerDao;
import com.iris.tda.model.Customer;
import com.iris.tda.model.CustomerServiceResponse;
import com.iris.tda.service.CustomerService;

/**
 * Implements all the the methods of CustomerService class 
 * by calling the CustomerDaoImpl class.
 * */
@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDaoImpl;
	
	private final String validationErrorName="ValidationError:Invalid Name";
	private final String validationErrorAccount="ValidationError:Invalid Account";
	private final String lengthValidationError="ValidationError:Account number is not of 16 digit:";
	private final String lengthValidationNameError="ValidationError:Name can not be null";
	private final String exceptionMessage="An Exception Occured";

		
	@Override
	public List<Customer> ViewAllCustomer() {
		try {
		 customerDaoImpl.ViewAllCustomer();
	}
		catch(Exception e) {
			e.printStackTrace();
		}
			return customerDaoImpl.ViewAllCustomer();
		}

	@Override
	public CustomerServiceResponse addCustomer(Customer customer) {
		CustomerServiceResponse response=new CustomerServiceResponse();
		String pattern="^([a-zA-Z])+(\\s)+[a-zA-Z]+$";
		String accountPattern="^[0-9]*$";
		
		if(!customer.getName().matches(pattern) ) {
			response.addError(validationErrorName+customer.getName());
		}
		if(customer.getName().length()==0) {
			response.addError(lengthValidationNameError);
		}
		 if(!customer.getAccountnumber().matches(accountPattern)) {
			response.addError(validationErrorAccount+customer.getAccountnumber());
		}
		 if(customer.getAccountnumber().length()!=16) {
			 response.addError(lengthValidationError+customer.getAccountnumber());
		 }
//		 else {
//			throw  new CustomerApiException();
//		 }
		 
		 if(!response.hasError()) {
			 try {
			customerDaoImpl.addCustomer(customer);
			 }
			 catch(Exception e) {
				 response.addError(exceptionMessage+e.getMessage());
			 }
			 
			 
		 }
		

		return response;
		
	}

	@Override
	public CustomerServiceResponse updateCustomer(Customer customer) {
		CustomerServiceResponse response=new CustomerServiceResponse();
		String pattern="^([a-zA-Z])+(\\s)+[a-zA-Z]+$";
		String accountPattern="^[0-9]*$";

		if(!customer.getName().matches(pattern)) {
			response.addError(validationErrorName);
		}
		if(customer.getName().length()==0) {
			response.addError(lengthValidationNameError);
		}
		 if(!customer.getAccountnumber().matches(accountPattern)) {
			response.addError(validationErrorAccount);
		}
		 if(customer.getAccountnumber().length()!=16) {
			 response.addError(lengthValidationError+customer.getAccountnumber());
		 }
		 
		 if(!response.hasError()) {
			 try {
			customerDaoImpl.updateCustomer(customer);
			 }
			 catch(Exception e) {
				 response.addError(exceptionMessage+e.getMessage());
			 }
		 }
			 return response;
			 
		 }

	@Override
	public CustomerServiceResponse deleteCustomer(int id) {
		CustomerServiceResponse response=new CustomerServiceResponse();
		
			try {
				customerDaoImpl.deleteCustomer(id);
			}
			catch(Exception e) {
				response.addError(exceptionMessage+e.getMessage());
			}
		
		return response;
	}

}
