//package com.iris.tda.config;
//
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//
//@EnableWebSecurity
//@EnableResourceServer
//
//public class CustomerDetailsUrlAuthentication extends ResourceServerConfigurerAdapter {
//	
//	@Override
//	public void configure(HttpSecurity http) throws Exception {
//		http
//			.authorizeRequests()
//				.antMatchers("/customers","/customer")
//				.permitAll()
//				.anyRequest().authenticated();
//	}
//
//}
