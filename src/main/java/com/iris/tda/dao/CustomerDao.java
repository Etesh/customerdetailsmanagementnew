package com.iris.tda.dao;

import java.util.List;

import com.iris.tda.model.Customer;

/**
 * This is dao interface  of CustomerDetails consists of methods used for performing CRUD operation.
 * **/
public interface CustomerDao {
	public List<Customer> ViewAllCustomer();

	public boolean addCustomer(Customer customer);

	public Customer getCustomerById(int id);

	public boolean updateCustomer(Customer customer);

	public boolean deleteCustomer(int id);
}
