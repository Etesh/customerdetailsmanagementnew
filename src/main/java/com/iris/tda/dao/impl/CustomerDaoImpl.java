package com.iris.tda.dao.impl;

import java.sql.ResultSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.iris.tda.dao.CustomerDao;
import com.iris.tda.model.Customer;

/**
 * This is repository class that is implementation of CustomerDao interface.
 * **/

@Repository
public class CustomerDaoImpl implements CustomerDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	private final String Get_All_Customer = "select * from CUSTOMER";
	private final String Insert_Customer = "insert into CUSTOMER(ID,NAME,AGE,ACCOUNTNUMBER,ADDRESS,ROLE) values(?,?,?,?,?,?)";
	private final String Update_Customer = "UPDATE CUSTOMER set NAME=?,AGE=?,ACCOUNTNUMBER=?,ADDRESS=?,ROLE=? WHERE ID=?";
	private final String Delete_Customer = "DELETE from CUSTOMER where id=?";

	/**
	 * RowMapper is an functional interface used by JdbcTemplate for mapping rows of a
	 * java.sql.ResultSet on a per-row basis. A ResultSet object maintains a cursor
	 * pointing to its current row of data Initially the cursor is positioned before
	 * the first row.
	 */

	private RowMapper<Customer> rowMapper = (ResultSet rs, int rowNumber) -> {
		Customer customer = new Customer();
		customer.setId(rs.getInt(1));;
		customer.setName(rs.getString(2));
		customer.setAge(rs.getInt(3));
		customer.setAccountnumber(rs.getString(4));
		customer.setAddress(rs.getString(5));
		customer.setRole(rs.getString(6));
		return customer;
	};

	/**
	 * This method is used to retrieve all the list of customer from database.
	 * 
	 * @rowMapper is a functional interface to fetch the records from the database
	 *            using query() method of JdbcTemplate class.
	 */

	@Override
	public List<Customer> ViewAllCustomer() {
		return jdbcTemplate.query(Get_All_Customer, rowMapper);

	}

	/**
	 * This Method insert or add Customer to database with basic Information or
	 * personal details of customer using update() method ofJdbcTemplate class.
	 * 
	 * @return boolean this return true if Customer added successfully otherwise
	 *         return false.
	 */

	@Override
	public boolean addCustomer(Customer customer) {
		if (jdbcTemplate.update(Insert_Customer, customer.getId(), customer.getName(), customer.getAge(), customer.getAccountnumber(),
				customer.getAddress(), customer.getRole()) > 0) {
			return true;
		} else
			return false;

	}

	@Override
	public Customer getCustomerById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This method update the details of customer using update() method of
	 * JdbcTemplate class.
	 * 
	 * @return boolean this return true if customer updated successfully otherwise
	 *         return false.
	 */

	@Override
	public boolean updateCustomer(Customer customer) {
		if (jdbcTemplate.update(Update_Customer, customer.getName(), customer.getAge(), customer.getAccountnumber(), customer.getAddress(),
				customer.getRole(), customer.getId()) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method delete or remove the customer from database using update() method
	 * of JdbcTemplate class.
	 * 
	 * @return boolean this return true if customer deleted successfully otherwise
	 *         return false.
	 */

	@Override
	public boolean deleteCustomer(int id) {
		if (jdbcTemplate.update(Delete_Customer, id) > 0) {
			return true;
		} else {
			return false;
		}
	}

}
