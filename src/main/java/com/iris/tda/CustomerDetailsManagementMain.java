package com.iris.tda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;


@SpringBootApplication

//@EnableAuthorizationServer

/**
 * This is main class of spring boot application.
 * **/

public class CustomerDetailsManagementMain {

	public static void main(String[] args) {
		SpringApplication.run(CustomerDetailsManagementMain.class, args);

	}

}
