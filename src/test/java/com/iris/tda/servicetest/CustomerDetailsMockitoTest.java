package com.iris.tda.servicetest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iris.tda.dao.impl.CustomerDaoImpl;
import com.iris.tda.model.Customer;
import com.iris.tda.model.CustomerServiceResponse;
import com.iris.tda.service.impl.CustomerServiceImpl;

/**
 * This class represent mock testing of the service class. 
 * **/
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerDetailsMockitoTest {

	@InjectMocks
	private CustomerServiceImpl customerService;

	@Mock
	private CustomerDaoImpl customerDaoImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testViewAllCustomer() {
		when(customerDaoImpl.ViewAllCustomer()).thenReturn(Stream
				.of(new Customer(3, "Raj", 22, "25800", "Saket,New Delhi", "Customer"),
						new Customer(8, "Prayas", 20, "8400", "Pune,Maharashtra", "Customer"))
				.collect(Collectors.toList()));
		assertEquals(2, customerService.ViewAllCustomer().size());
	}

	@Test
	public void testAddCustomer() {
		
//		Customer customer=new Customer(9, "Amardeep", 29, "1234567891234567", "Mukherjee nagar,New Delhi", "Customer");
//		when(customerDaoImpl.addCustomer(customer)).thenReturn(true);
//		assertEquals(customer, customerDaoImpl.addCustomer(customer));
//		
		CustomerServiceResponse response=new CustomerServiceResponse();
		String pattern="^([a-zA-Z])+(\\s)+[a-zA-Z]+$";
		String accountPattern="^[0-9]*$";
		Customer cust = new Customer(9, "Amardeep", 29, "1234567891234567", "Mukherjee nagar,New Delhi", "Customer");
		when(cust.getName().matches(pattern) && cust.getAccountnumber().matches(accountPattern) && customerDaoImpl.addCustomer(cust)).thenReturn(true);
		assertEquals(!response.hasError(), customerService.addCustomer(cust));
	}

	@Test
	public void testUpdatecustomer() {
		Customer customer = new Customer();
		customer.setId(9);
		customer.setName("Amardeep kumar");
		customer.setAge(30);
		customer.setAccountnumber("1234567891234567");
		customer.setAddress("Saket,New Delhi");
		customer.setRole("Customer");
		customerService.updateCustomer(customer);
		verify(customerDaoImpl, times(1)).updateCustomer(customer);
	}

	@Test
	public void testDeleteCustomer() {
		Customer customer = new Customer(9, "Amardeep", 29, "18009", "Mukherjee nagar,New Delhi", "Customer");
		customerDaoImpl.addCustomer(customer);
		int id = 9;
		customerService.deleteCustomer(id);
		verify(customerDaoImpl, times(1)).deleteCustomer(id);

	}

}
