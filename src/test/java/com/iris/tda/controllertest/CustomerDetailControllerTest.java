package com.iris.tda.controllertest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hibernate.sql.Update;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.iris.tda.controller.impl.CustomerDetailsController;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerDetailsController.class)

public class CustomerDetailControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private CustomerDetailsController customerDetailsController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testWrongUrl() throws Exception {
		mvc.perform(get("/customerss").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testViewAllCustomer() throws Exception {
		mvc.perform(get("/customers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void testAddCustomer() throws Exception {
		mvc.perform(post("/customer").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void testUpdateCustomer() throws Exception {
		mvc.perform(put("/customer").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	@Test
	public void testDeleteCustomer() throws Exception {
		mvc.perform(delete("/customer").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden());
	}

//	@Test
//	public void testViewAllCustomer() throws Exception{
//		mockMvc.perform(MockMvcRequestBuilders.get("/customers")
//				).andExpect(MockMvcResultMatchers.status().isOk())
//				.andExpect(jsonPath("$", ));
//		verify(customerDetailsController).ViewAllCustomer();
//	}

}
