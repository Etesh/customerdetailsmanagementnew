package com.iris.tda.daoimpl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iris.tda.dao.impl.CustomerDaoImpl;
import com.iris.tda.model.Customer;

/**
 * This class  represent unit testing of repository class.
 * **/

@RunWith(SpringJUnit4ClassRunner.class)
class CustomerDaoImplTest {

	@Autowired
	private CustomerDaoImpl dao;

	@BeforeEach
	public void setUp() throws Exception {
		DriverManagerDataSource datasource=new DriverManagerDataSource();
		datasource.setUrl("jdbc:h2:~/test");
		datasource.setUsername("sa");
		datasource.setPassword("");

	}

	@Test
	 public void testViewAllCustomer() {
		List<Customer> list = dao.ViewAllCustomer();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}

	@Test
	public void testAddCustomer() {
		Customer customer = new Customer(5, "Vishal Srivastava", 24, "1234567891234567", "Gorakhpur,U.P", "Customer");
		dao.addCustomer(customer);
	}

	@Test
	public void testUpdateCustomer() {
		Customer cust = new Customer();
		cust.setId(2);
		cust.setName("Aman singh rajput");
		cust.setAge(24);
		cust.setAccountnumber("81000");
		cust.setAddress("Gaautam nagar,U.P,G.Noida");
		cust.setRole("Customer");
		dao.updateCustomer(cust);
	}

	@Test
	public void testDeleteCustomer() {
		Customer customer = new Customer(5, "Vishal Srivastava", 24, "1234567891234567", "Gorakhpur,U.P", "Customer");
		dao.addCustomer(customer);
		int id = 5;
		dao.deleteCustomer(id);
	}

}
